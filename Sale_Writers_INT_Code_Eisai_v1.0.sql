drop table if exists eisai_dev_db.int_sales_writer;
create table eisai_dev_db.int_sales_writer as select * from
(
-- TRx Tile and SP Graph

select
 brand_name, Grouped_Indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
10 as Panel_Id
,'Aggregate'  as Panel_Name
,100 as category_id
,'TRx' as category_name
,cast(concat('100',cast(dense_rank() over (partition by null order by channel) as varchar) )as int) as subcategory_id

,channel as Subcategory_Name
,10001 as Metric_Id
,'TRx' as Metric_Name
,sum(curr_trx) as Metric_Val1
,0 as Metric_Val2
from eisai_dev_db.idl_eisai_sales_writer 
where grouped_indication = indication
group by  brand_name, Grouped_Indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,channel


union all

-- NRx  
select
 brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
10 as Panel_Id
,'Aggregate'  as Panel_Name
,101 as category_id
,'NRx' as category_name
,cast(concat('101',cast(dense_rank() over (partition by null order by channel) as varchar) )as int) as subcategory_id

,channel as Subcategory_Name
,10002 as Metric_Id
,'NRx' as Metric_Name
,sum(curr_nrx) as Metric_Val1
,0 as Metric_Val2
from eisai_dev_db.idl_eisai_sales_writer 
where grouped_indication = indication
group by  brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,channel


union all


-- TRx Growth 
select
 brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
10 as Panel_Id
,'Aggregate'  as Panel_Name
,102 as category_id
,'TRx' as category_name
, 1021 as subcategory_id

, 'TRx_Growth' as Subcategory_Name
,10003 as Metric_Id
,'TRx Growth' as Metric_Name
,case when cast(sum(prev_trx) as decimal(22,7)) = 0 then NULL
else cast(sum(trx_diff) as decimal(22,7)) /cast(sum(prev_trx) as decimal(22,7)) end as Metric_Val1
,0 as Metric_Val2
from eisai_dev_db.idl_eisai_sales_writer 
where grouped_indication = indication
group by  brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end





union all

-- NRx Growth 
select
 brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
10 as Panel_Id
,'Aggregate'  as Panel_Name
,103 as category_id
,'NRx' as category_name
,1031 as subcategory_id

,'NRx_Growth' as Subcategory_Name
,10004 as Metric_Id
,'NRx Growth' as Metric_Name
,case when cast(sum(prev_nrx) as decimal(22,7))=0 then NULL
else cast(sum(nrx_diff) as decimal(22,7)) /cast(sum(prev_nrx) as decimal(22,7)) end as Metric_Val1
,0 as Metric_Val2
from eisai_dev_db.idl_eisai_sales_writer 
where grouped_indication = indication
group by  brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end



union all

-- TOTAL WRITERS

select
 brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
10 as Panel_Id
,'Aggregate'  as Panel_Name
,104 as category_id
,'Total_Writers' as category_name
,1041 as subcategory_id

, 'Total_Writers' as Subcategory_Name
,10005 as Metric_Id
,'Total_Writers' as Metric_Name
,MAX(total_writer) as Metric_Val1  -- Check column name
,0 as Metric_Val2
from eisai_dev_db.idl_eisai_sales_writer 
where src_cd = 'Sp' and grouped_indication = indication
group by  brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end



union all

-- NEW WRITERS

select
 brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
10 as Panel_Id
,'Aggregate'  as Panel_Name
,105 as category_id
,'New_Writers' as category_name
,1051 as subcategory_id

, 'New_Writers' as Subcategory_Name
,10006 as Metric_Id
,'New_Writers' as Metric_Name
,MAX(new_writer) as Metric_Val1  -- Check column name
,0 as Metric_Val2
from eisai_dev_db.idl_eisai_sales_writer 
where src_cd = 'Sp' and grouped_indication = indication
group by  brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end




union all

--- TOTAL_WRITERS_GROWTH
select
 brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
10 as Panel_Id
,'Aggregate'  as Panel_Name
,106 as category_id
,'Total_Writers_Growth' as category_name
,1061 as subcategory_id

,'Total_Writers_Growth' as Subcategory_Name
,10005 as Metric_Id
,'Total_Writers_Growth' as Metric_Name
,MAX(total_writer_growth) as Metric_Val1
,0 as Metric_Val2
from eisai_dev_db.idl_eisai_sales_writer 
where src_cd = 'Sp' and grouped_indication = indication
group by  brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end




union all

-- NEW WRITERS GROWTH

select
 brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
10 as Panel_Id
,'Aggregate'  as Panel_Name
,107 as category_id
,'New_Writers' as category_name
,1071 as subcategory_id

, 'New_Writers_Growth' as Subcategory_Name
,10006 as Metric_Id
,'New_Writers_Growth' as Metric_Name
,MAX(new_writer_growth) as Metric_Val1  -- Check column name
,0 as Metric_Val2
from eisai_dev_db.idl_eisai_sales_writer 
where src_cd = 'Sp' and  grouped_indication = indication
group by  brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end






union all




--- TRX By GROUPED INDICATION

select
brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
20 as Panel_Id
,'Performance By Indication' as Panel_Name
,200 as category_id
,'Grouped_Indication' as category_name
,cast(concat('200',cast(dense_rank() over (partition by null order by indication) as varchar) )as int) as subcategory_id
,indication as Subcategory_Name
,20001 as Metric_Id
,'TRx' as Metric_Name
,sum(curr_trx) as Metric_Val1
,case 
	when (upper(grouped_indication) = 'ALLIANCE' and upper(indication) = 'ALLIANCE') 
	or (upper(grouped_indication) = 'ALL' and upper(indication) = 'ALL') 
	then 0
	else 1
end as MetricVal2
	
from eisai_dev_db.idl_eisai_sales_writer
group by brand_name, indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end, grouped_indication




union all






----- TRx Growth by Grouped_Indication

select
 brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
20 as Panel_Id
,'Performance By Indication'  as Panel_Name
,201 as category_id
,'Grouped_Indication' as category_name
,cast(concat('201',cast(dense_rank() over (partition by null order by indication) as varchar) )as int) as subcategory_id
,indication as Subcategory_Name
,20001 as Metric_Id
,'TRx Growth' as Metric_Name
,case when cast(sum(prev_trx) as decimal(22,7)) = 0 then NULL
else cast(sum(trx_diff) as decimal(22,7)) /cast(sum(prev_trx) as decimal(22,7)) end as Metric_Val1
,case 
	when (upper(grouped_indication) = 'ALLIANCE' and upper(indication) = 'ALLIANCE') 
	or (upper(grouped_indication) = 'ALL' and upper(indication) = 'ALL') 
	then 0
	else 1
end Metric_Val2

from eisai_dev_db.idl_eisai_sales_writer 
group by  brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end, indication



union all



-- NRx by Grouped_Indication 
select
 brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
20 as Panel_Id
,'Performance By Indication'  as Panel_Name
,202 as category_id
,'Grouped_Indication' as category_name
,cast(concat('202',cast(dense_rank() over (partition by null order by indication) as varchar) )as int) as subcategory_id
,indication as Subcategory_Name
,20002 as Metric_Id
,'NRx' as Metric_Name
,sum(curr_nrx) as Metric_Val1
,case 
	when (upper(grouped_indication) = 'ALLIANCE' and upper(indication) = 'ALLIANCE') 
	or (upper(grouped_indication) = 'ALL' and upper(indication) = 'ALL') 
	then 0
	else 1
end as MetricVal2
from eisai_dev_db.idl_eisai_sales_writer 
group by  brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end, indication



union all




-- NRx Growth by Grouped_Indication 
select
 brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
20 as Panel_Id
,'Performance By Indication'  as Panel_Name
,203 as category_id
,'Grouped_Indication' as category_name
,cast(concat('203',cast(dense_rank() over (partition by null order by indication) as varchar) )as int) as subcategory_id
,indication as Subcategory_Name
,20003 as Metric_Id
,'NRx Growth' as Metric_Name
,case when cast(sum(prev_nrx) as decimal(22,7))=0 then NULL
else cast(sum(nrx_diff) as decimal(22,7)) /cast(sum(prev_nrx) as decimal(22,7)) end as Metric_Val1
,case 
	when (upper(grouped_indication) = 'ALLIANCE' and upper(indication) = 'ALLIANCE') 
	or (upper(grouped_indication) = 'ALL' and upper(indication) = 'ALL') 
	then 0
	else 1
end Metric_Val2
from eisai_dev_db.idl_eisai_sales_writer 
group by  brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end, indication



union all


-- TOTAL WRITERS by Grouped_Indication

select
 brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
20 as Panel_Id
,'Performance By Indication'  as Panel_Name
,204 as category_id
,'Grouped_Indication' as category_name
,cast(concat('204',cast(dense_rank() over (partition by null order by indication) as varchar) )as int) as subcategory_id

,indication as Subcategory_Name
,20004 as Metric_Id
,'Total_Writers' as Metric_Name
,MAX(total_writer) as Metric_Val1  
,case 
	when (upper(grouped_indication) = 'ALLIANCE' and upper(indication) = 'ALLIANCE') 
	or (upper(grouped_indication) = 'ALL' and upper(indication) = 'ALL') 
	then 0
	else 1
end Metric_Val2
from eisai_dev_db.idl_eisai_sales_writer 
where src_cd = 'Sp'
group by  brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end, indication




union all




-- NEW WRITERS

select
 brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
20 as Panel_Id
,'Performance By Indication'  as Panel_Name
,205 as category_id
,'Grouped_Indication' as category_name
,cast(concat('205',cast(dense_rank() over (partition by null order by indication) as varchar) )as int) as subcategory_id

, indication as Subcategory_Name
,20005 as Metric_Id
,'New_Writers' as Metric_Name
,MAX(new_writer) as Metric_Val1  -- Check column name
,case 
	when (upper(grouped_indication) = 'ALLIANCE' and upper(indication) = 'ALLIANCE') 
	or (upper(grouped_indication) = 'ALL' and upper(indication) = 'ALL') 
	then 0
	else 1
end Metric_Val2
from eisai_dev_db.idl_eisai_sales_writer 
where src_cd = 'Sp'
group by  brand_name, grouped_indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end, indication



union all

-- TRx by SRC


select
 brand_name, Grouped_Indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,
30 as Panel_Id
,'Performance By Source'  as Panel_Name
,301 as category_id
,'TRx By Source' as category_name
,cast(concat('301',cast(dense_rank() over (partition by null order by src_cd_chnl) as varchar) )as int) as subcategory_id

,src_cd_chnl as Subcategory_Name
,30001 as Metric_Id
,'TRx' as Metric_Name
,sum(curr_trx) as Metric_Val1
,0 as Metric_Val2
from (select *
,(src_cd ||'-'|| channel) as src_cd_chnl 

from eisai_dev_db.idl_eisai_sales_writer ) as sls
where grouped_indication = indication
group by  brand_name, Grouped_Indication, time_bckt_nm, time_bckt_cd, time_bckt_strt, time_bckt_end,src_cd_chnl

);
















