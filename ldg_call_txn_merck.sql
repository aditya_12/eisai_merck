drop table if exists eisai_prod_db.ldg_call_txn_merck;
create table eisai_prod_db.ldg_call_txn_merck
(
PARTY_ID	varchar(255)
,FIRST_NM	varchar(255)
,LAST_NM	varchar(255)
,ADDR_LN_1_TXT	varchar(255)
,IMS_ALTRNT_ID	varchar(255)
,NPI_ALTRNT_ID	varchar(255)
,CITY_NM	varchar(255)
,STATE_CD	varchar(255)
,ZIP_CD	varchar(255)
,MSTR_PROD_NM	varchar(255)
,CALL_PROD_DTL_SEQ_NMBR	bigint
,DATE	varchar(255)
,YEAR	varchar(255)
,MONTH	bigint
,DAY	bigint
,MRKTNG_FLAG_NM	varchar(255)
,FLAG_SEGMENT	varchar(255)
,CALLS	bigint
,POSITION	bigint
,PartyID_Prod_Date	varchar(255)
,PSTN_CD	varchar(255)
,ORG_UNIT_CD	varchar(255)
,ONE_DAY_COUNT	varchar(255)
,Eligible_Call	varchar(255)
,call_id	varchar(255)
,call_topic_desc	varchar(255)
);


TRUNCATE table eisai_prod_db.ldg_call_txn_merck;


copy eisai_prod_db.ldg_call_txn_merck 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/in/ZS_Soure_Tables/calls/merck/merck_20211015/Lenvima Weekly Transaction File - WO 10152021.csv' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
FORMAT as CSV
IGNOREHEADER 1
;
