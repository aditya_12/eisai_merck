drop table if exists eisai_prod_db.idl_call_activity_int1;
create table eisai_prod_db.idl_call_activity_int1 as select * from
(
select org, brand_name, product_position, mdm_call_channel,  time_bckt_cd, target_flag,indication,grouped_indication, time_bckt_strt, time_bckt_end, time_bckt_nm, time_bckt_freq
,call_details
,hcp_tgt_cnt as target_reach
,hcp_tgt_cnt_channel as target_reach_channel
,segment
,early_adopter
,hcp_tgt_ind_cnt
, case when cast(max(hcp_tgt_cnt) over (partition by org, brand_name, indication, grouped_indication,  time_bckt_cd,target_flag) as decimal(22,7)) = 0 then 0 else cast(sum(call_details)  over (partition by org, brand_name,indication, grouped_indication,  time_bckt_cd,target_flag)  as decimal(22,7))
/
cast(max(hcp_tgt_cnt) over (partition by org, brand_name, indication, grouped_indication,  time_bckt_cd,target_flag) as decimal(22,7)) end as avg_tgt_freq

,case when cast(max(hcp_tgt_ind_cnt) over (partition by org, brand_name, indication, grouped_indication,  time_bckt_cd,target_flag) as decimal(22,7)) = 0 then 0 
else cast(sum(call_details)  over (partition by org, brand_name,indication, grouped_indication,  time_bckt_cd,target_flag)  as decimal(22,7))
/
cast(max(hcp_tgt_ind_cnt) over (partition by org, brand_name, indication, grouped_indication,  time_bckt_cd,target_flag) as decimal(22,7)) end as avg_tgt_ind_freq

,target_count
,case when cast(target_count as decimal(22,7)) = 0 then 0
else cast(max(hcp_tgt_cnt) over (partition by org, brand_name, grouped_indication,  time_bckt_cd,target_flag) as decimal(22,7))/cast(target_count as decimal(22,7)) end as target_reach_ratio

,case when cast(target_count as decimal(22,7)) = 0 then 0
else cast(max(hcp_tgt_cnt_channel) over (partition by org, brand_name, grouped_indication,  time_bckt_cd,target_flag,mdm_call_channel) as decimal(22,7))/cast(target_count as decimal(22,7)) end as target_reach_ratio_channel

,case when cast(target_count as decimal(22,7)) = 0 then 0
else cast(max(hcp_tgt_ind_cnt) over (partition by org, brand_name, indication, grouped_indication,  time_bckt_cd,target_flag) as decimal(22,7))/cast(target_count as decimal(22,7)) end as target_reach_ind_ratio

from 
(
select distinct org, brand_name, product_position, mdm_call_channel,  time_bckt_cd, target_flag,indication, grouped_indication, time_bckt_strt, time_bckt_end, time_bckt_nm, time_bckt_freq, segment, early_adopter

,sum(total_calls) over(partition by org, brand_name, product_position, mdm_call_channel,  time_bckt_cd, time_bckt_strt, target_flag,indication, grouped_indication, segment, early_adopter) as call_details

,regexp_count(
   listaggdistinct(hcp_id, ',') over (partition by org, brand_name, indication, grouped_indication,  time_bckt_cd,target_flag), 
   ','
) + 1 as hcp_tgt_ind_cnt

,regexp_count(
   listaggdistinct(hcp_id, ',') over (partition by org, brand_name, grouped_indication,  time_bckt_cd,target_flag), 
   ','
) + 1 as hcp_tgt_cnt
,target_count

,regexp_count(
   listaggdistinct(hcp_id, ',') over (partition by org, brand_name, grouped_indication,  time_bckt_cd,target_flag,mdm_call_channel), 
   ','
) + 1 as hcp_tgt_cnt_channel

from eisai_prod_db.f_call_activity
)fnl
);


-----------------------************-----------------------

drop table if exists eisai_prod_db.idl_call_activity;
create table eisai_prod_db.idl_call_activity as
select idl_int1.* , seg_cnt.hcp_tgt_cnt_by_segment, seg_ea_cnt.hcp_tgt_cnt_by_segment_ea
,seg_cnt.target_seg_count
,seg_ea_cnt.target_seg_ea_count

,case when cast(target_seg_count as decimal(22,7)) = 0 then 0
else cast(max(hcp_tgt_cnt_by_segment) over (partition by idl_int1.org, idl_int1.brand_name, idl_int1.grouped_indication, idl_int1.time_bckt_cd,idl_int1.segment) as decimal(22,7))/cast(target_seg_count as decimal(22,7)) end as target_reach_ratio_by_target

,case when cast(target_seg_ea_count as decimal(22,7)) = 0 then 0
else cast(max(hcp_tgt_cnt_by_segment_ea) over (partition by idl_int1.org, idl_int1.brand_name, idl_int1.grouped_indication, idl_int1.time_bckt_cd,idl_int1.segment) as decimal(22,7))/cast(target_seg_ea_count as decimal(22,7)) end as target_reach_ratio_by_ea
from eisai_prod_db.idl_call_activity_int1 as idl_int1

left join
(select distinct
org, brand_name, grouped_indication,  time_bckt_cd, segment, indication
,regexp_count(
   listaggdistinct(hcp_id, ',') over (partition by org, brand_name, grouped_indication,  time_bckt_cd, segment), 
   ','
) + 1 as hcp_tgt_cnt_by_segment
,target_seg_count
from eisai_prod_db.f_call_activity
) as seg_cnt
on  seg_cnt.org                 = idl_int1.org
and seg_cnt.brand_name          = idl_int1.brand_name
and seg_cnt.grouped_indication  = idl_int1.grouped_indication
and seg_cnt.indication          = idl_int1.indication
and seg_cnt.time_bckt_cd        = idl_int1.time_bckt_cd
and seg_cnt.segment             = idl_int1.segment

left join
(select distinct
org, brand_name, grouped_indication,  time_bckt_cd, segment, indication
,regexp_count(
   listaggdistinct(hcp_id, ',') over (partition by org, brand_name, grouped_indication,  time_bckt_cd, segment), 
   ','
) + 1 as hcp_tgt_cnt_by_segment_ea
,target_seg_ea_count
from eisai_prod_db.f_call_activity
where early_adopter='Y'
) as seg_ea_cnt
on  seg_ea_cnt.org                 = idl_int1.org
and seg_ea_cnt.brand_name          = idl_int1.brand_name
and seg_ea_cnt.grouped_indication  = idl_int1.grouped_indication
and seg_ea_cnt.indication          = idl_int1.indication
and seg_ea_cnt.time_bckt_cd        = idl_int1.time_bckt_cd
and seg_ea_cnt.segment             = idl_int1.segment;





