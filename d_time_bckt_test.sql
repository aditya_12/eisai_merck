drop table if exists eisai_dev_db.d_time_bckt_test;

create table eisai_dev_db.d_time_bckt_test as

select source,time_bckt_freq,to_date(time_bckt_strt, 'YYYY-MM-DD 00:00:00') as time_bckt_strt,to_date(time_bckt_end, 'YYYY-MM-DD 00:00:00' )  as time_bckt_end,cycl_time_id,recency,time_bckt_nm,time_bckt_cd,indication from eisai_dev_db.d_time_bckt_activity_temp
union all
select source,time_bckt_freq,to_date(time_bckt_strt, 'YYYY-MM-DD 00:00:00') as time_bckt_strt,to_date(time_bckt_end, 'YYYY-MM-DD 00:00:00' ) as time_bckt_end,cycl_time_id,recency,time_bckt_nm,time_bckt_cd,indication from eisai_dev_db.d_time_bckt_sales_temp
