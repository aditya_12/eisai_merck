drop table if exists eisai_dev_db.ldg_call_targets_merck;
create table eisai_dev_db.ldg_call_targets_merck
(
Indication                        varchar(255)                                                                                 
,NPI				              varchar(255)
,Segment                     	  varchar(255)                                                        
,EC_RCC_Early_Adopter             varchar(255)                                                                                                                                                               
);

copy eisai_dev_db.ldg_call_targets_merck 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/in/ZS_Soure_Tables/ldg_call_targets_merck.csv' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
FORMAT as CSV
IGNOREHEADER 1
;

--old
DO NOT SEE THIS
SEE BELOW TWO TABLES
=============

--RUN THESE TWO TABLES

drop table if exists eisai_prod_db.ldg_call_targets_merck;
create table eisai_prod_db.ldg_call_targets_merck
(
NPI_ID                        varchar(255)                                                                                 
,IMS_ID				              varchar(255)
,Segment                     	  varchar(255)                                                        
,Indication             varchar(255)                                                                                                                                                               
);

copy eisai_prod_db.ldg_call_targets_merck 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/in/ZS_Soure_Tables/call_targets_merck.csv' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
FORMAT as CSV
IGNOREHEADER 1
;

------------------***********-------------------

drop table if exists eisai_prod_db.ldg_call_targets_merck_ea;
create table eisai_prod_db.ldg_call_targets_merck_ea
(
NPI_ID                      varchar(255)                                                                                 
,IMS_ID				              varchar(255)                                                       
,Indication             varchar(255)                                                                                                                                                               
);

copy eisai_prod_db.ldg_call_targets_merck_ea 
from 's3://aws-a0077-use1-00-p-s3b-tyo-shr-int-tyo-tdec01/in/ZS_Soure_Tables/call_targets_merck_ea.csv' 
IAM_ROLE 'arn:aws:iam::345666082227:role/aws-a0077-glbl-00-p-rol-shrd-awb-shrd-prod_173'
FORMAT as CSV
IGNOREHEADER 1
;
