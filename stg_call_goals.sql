drop table if exists eisai_prod_db.stg_call_goals;
create table  eisai_prod_db.stg_call_goals as 

select 'EISAI' as org, prorated_call.*, FYTD_time_bckt_strt, day_diff*(35000/365) as prorated_calls
from 
(
select all_time_bckt.*, FYTD_time_bckt_strt
,1+datediff(day,case when to_date(time_bckt_strt,'YYYY-MM-DD')<FYTD_time_bckt_strt then FYTD_time_bckt_strt else to_date(time_bckt_strt,'YYYY-MM-DD') end
,to_date(time_bckt_end,'YYYY-MM-DD')
)as day_diff 
from 
(
select *
from eisai_dev_db.d_time_bckt where source='Activity' 
and time_bckt_cd in ( 'FYTD1 (W)', 'WTD', 'R13W', 'R4W', 'M1', 'R12M', 'MTD', 'YTD (W)', 'QTD1 (W)')
) as all_time_bckt
join 
(select distinct to_date(time_bckt_strt,'YYYY-MM-DD') as FYTD_time_bckt_strt from eisai_dev_db.d_time_bckt where source='Activity' 
and time_bckt_cd='FYTD1 (W)' ) as fytd
on 1=1
) as prorated_call

union all

select 'MERCK' as org,prorated_call.*,FYTD_time_bckt_strt,day_diff*(35000/365) as prorated_calls
from 
(
select all_time_bckt.*,FYTD_time_bckt_strt
,1+datediff(day,case when to_date(time_bckt_strt,'YYYY-MM-DD')<FYTD_time_bckt_strt then FYTD_time_bckt_strt else to_date(time_bckt_strt,'YYYY-MM-DD') end
,to_date(time_bckt_end,'YYYY-MM-DD')
)as day_diff 
from 
(
select *
from eisai_dev_db.d_time_bckt where source='Activity' 
and time_bckt_cd in ( 'FYTD1 (W)', 'WTD', 'R13W', 'R4W', 'M1', 'R12M', 'MTD', 'YTD (W)', 'QTD1 (W)')
) as all_time_bckt
join 
(select distinct to_date(time_bckt_strt,'YYYY-MM-DD') as FYTD_time_bckt_strt from eisai_dev_db.d_time_bckt where source='Activity' 
and time_bckt_cd='FYTD1 (W)' ) as fytd
on 1=1
) as prorated_call

union all

select 'ALLIANCE' as org,prorated_call.*,FYTD_time_bckt_strt,day_diff*(70000/365) as prorated_calls
from 
(
select all_time_bckt.*,FYTD_time_bckt_strt

,1+datediff(day,case when to_date(time_bckt_strt,'YYYY-MM-DD')<FYTD_time_bckt_strt then FYTD_time_bckt_strt else to_date(time_bckt_strt,'YYYY-MM-DD') end
,to_date(time_bckt_end,'YYYY-MM-DD')
)as day_diff 
from 
(
select *
from eisai_dev_db.d_time_bckt where source='Activity' 
and time_bckt_cd in ( 'FYTD1 (W)', 'WTD', 'R13W', 'R4W', 'M1', 'R12M', 'MTD', 'YTD (W)', 'QTD1 (W)')
) as all_time_bckt
join 
(select distinct to_date(time_bckt_strt,'YYYY-MM-DD') as FYTD_time_bckt_strt from eisai_dev_db.d_time_bckt where source='Activity' 
and time_bckt_cd='FYTD1 (W)' ) as fytd
on 1=1
) as prorated_call;


--------------------------*********---------------------











