drop table if exists eisai_prod_db.stg_call_targets;
create table eisai_prod_db.stg_call_targets as
(
select distinct eisai_targets.npi_id, LPAD(eisai_targets.ims_id,7,'0000000') as ims_id, eisai_targets.segment, eisai_targets.indication, eisai_targets.Early_Adopter
,'EISAI' as org_target from eisai_prod_db.ldg_call_targets_eisai as eisai_targets where upper(trim(eisai_targets.segment)) in ('A', 'B', 'C')

UNION ALL

select distinct merck_targets.npi_id, LPAD(merck_targets.ims_id,7,'0000000') as ims_id, merck_targets.segment
,case when UPPER(trim(merck_targets.indication)) = 'ENDO' then 'EC'
else merck_targets.indication end as indication
,COALESCE(merck_targets_ea.Early_Adopter, 'N') as Early_Adopter, 'MERCK' as org_target 

from eisai_prod_db.ldg_call_targets_merck as merck_targets

left join

(select distinct npi_id, LPAD(ims_id,7,'0000000') as ims_id
,UPPER(trim(indication)) as indication
,'Y' as Early_Adopter from eisai_prod_db.ldg_call_targets_merck_ea) as merck_targets_ea
on merck_targets.npi_id = merck_targets_ea.npi_id and merck_targets.ims_id = merck_targets_ea.ims_id and merck_targets.indication = merck_targets_ea.indication

UNION ALL
(
select distinct EISAI.npi_id, EISAI.ims_id, EISAI.segment, EISAI.indication
,case when EISAI.early_adopter = 'Y' and MERCK.early_adopter = 'Y' then 'Y'
else 'N' end as early_adopter, EISAI.org_target from
(
select distinct eisai_targets.npi_id, LPAD(eisai_targets.ims_id,7,'0000000') as ims_id, 'ALLIANCE TARGETS' as segment, eisai_targets.indication, eisai_targets.Early_Adopter
,'ALLIANCE' as org_target from eisai_prod_db.ldg_call_targets_eisai as eisai_targets where upper(trim(eisai_targets.segment)) in ('A', 'B', 'C')
) EISAI

inner join

(
select distinct merck_targets.npi_id, LPAD(merck_targets.ims_id,7,'0000000') as ims_id, 'ALLIANCE TARGETS' as segment
,case when UPPER(trim(merck_targets.indication)) = 'ENDO' then 'EC'
else merck_targets.indication end as indication
,COALESCE(merck_targets_ea.Early_Adopter, 'N') as Early_Adopter, 'ALLIANCE' as org_target 
from eisai_prod_db.ldg_call_targets_merck as merck_targets
left join
(select distinct npi_id, LPAD(ims_id,7,'0000000') as ims_id
,UPPER(trim(indication)) as indication
,'Y' as Early_Adopter from eisai_prod_db.ldg_call_targets_merck_ea) as merck_targets_ea
on merck_targets.npi_id = merck_targets_ea.npi_id and merck_targets.ims_id = merck_targets_ea.ims_id and merck_targets.indication = merck_targets_ea.indication
) MERCK
on EISAI.ims_id = MERCK.ims_id and EISAI.indication=MERCK.INDICATION
)

);

