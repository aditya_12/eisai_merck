------ D TIME BUCKET SALES AUTOMATION -------


drop table if exists eisai_dev_db.d_time_bckt_activity_temp;
create table eisai_dev_db.d_time_bckt_activity_temp as 
select * from 
(



-----WEEKLY ----------

select wkly_rnk.*
,CONCAT('WEEK',RECENCY) as time_bckt_nm -- check exact name
,CONCAT('W',RECENCY) as time_bckt_cd
,'ANY' as indication
from 
(
select wkly.*,rank() over (order by  time_bckt_strt desc ) as recency
from 
(select distinct 'Activity' as source, 'WEEKLY' as time_bckt_freq, d.CAL_WK_STRTDT as time_bckt_strt , d.CAL_WK_ENDDT as time_bckt_end
,c.cycl_time_id
from  eisai_dev_db.d_time d 
join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')

between to_date(dateadd(day,-107*7,to_date(activity_feed,'YYYY-MM-DD', FALSE)),'YYYY-MM-DD 00:00:00')
 and to_date(activity_feed,'YYYY-MM-DD', FALSE) 
) as wkly
) as wkly_rnk



UNION ALL


-------MONTHLY----------


select mnthly_rnk.*
,CONCAT('MONTH ',RECENCY) as time_bckt_nm -- check exact name
,CONCAT('M',RECENCY) as time_bckt_cd
,'ANY' as indication
from 
(
select mnthly.*,rank() over (order by  time_bckt_strt desc ) as recency
from 
(
select distinct 'Activity' as source, 'MONTHLY' as time_bckt_freq, d.CAL_MTH_STRTDT as time_bckt_strt , d.CAL_MTH_ENDDT as time_bckt_end,c.cycl_time_id 
from eisai_dev_db.flg_sls as flg
cross join  eisai_dev_db.d_time d  
inner join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') 
between to_date(dateadd(month,(-36+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
and to_date(dateadd(month,(-1+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00')   --where c.ACTV_FLG='Y'
) as mnthly
) as mnthly_rnk

UNION ALL



------ R13W --------


select 'Activity' as source,'R13W' as time_bckt_freq,
cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id
,1 as recency
,'ROLLING 13 WEEKS' as time_bckt_nm
,'R13W' as time_bckt_cd

,'ANY' as indication
from
(select distinct 'R13W' as cd, d.CAL_WK_STRTDT as time_bckt_strt , d.CAL_WK_ENDDT as time_bckt_end,c.cycl_time_id  
from eisai_dev_db.d_time d inner join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(day,-13*7,to_date(activity_feed,'YYYY-MM-DD', FALSE)),'YYYY-MM-DD 00:00:00') and to_date(activity_feed,'YYYY-MM-DD', FALSE) 
)
group by cycl_time_id


UNION ALL




------ R13W (P)-------


select 'Sales' as source,'R13W' as time_bckt_freq,
cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id
,1 as recency
,'ROLLING 13 WEEKS PREVIOUS' as time_bckt_nm
,'R13W(P)' as time_bckt_cd

,'ANY' as indication
from
(select distinct 'R13W' as cd, d.CAL_WK_STRTDT as time_bckt_strt , d.CAL_WK_ENDDT as time_bckt_end,c.cycl_time_id  
from eisai_dev_db.d_time d inner join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(day,-26*7,to_date(activity_feed,'YYYY-MM-DD', FALSE)),'YYYY-MM-DD 00:00:00') and  to_date(dateadd(day,-13*7,to_date(activity_feed,'YYYY-MM-DD', FALSE)),'YYYY-MM-DD 00:00:00')
)
group by cycl_time_id



UNION ALL


------ R4W --------



select 'Activity' as source,'R4W' as time_bckt_freq
,cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'ROLLING 4 WEEKS' as time_bckt_nm
,'R4W' as time_bckt_cd
,'ANY' as indication
from
(select distinct 'R4W' as cd, d.CAL_WK_STRTDT as time_bckt_strt , d.CAL_WK_ENDDT as time_bckt_end,c.cycl_time_id  
from eisai_dev_db.d_time d inner join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(day,-3*7,to_date(activity_feed,'YYYY-MM-DD', FALSE)),'YYYY-MM-DD 00:00:00') and to_date(activity_feed,'YYYY-MM-DD', FALSE) 
)
group by cycl_time_id




UNION ALL






------ R4W(P)--------

select 'Sales' as source,'R4W' as time_bckt_freq,
cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'ROLLING 4 WEEKS PREVIOUS' as time_bckt_nm
,'R4W(P)' as time_bckt_cd
,'ANY' as indication
from
(select distinct 'R4W' as cd, d.CAL_WK_STRTDT as time_bckt_strt , d.CAL_WK_ENDDT as time_bckt_end,c.cycl_time_id  
from eisai_dev_db.d_time d inner join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(day,-6*7,to_date(activity_feed,'YYYY-MM-DD', FALSE)),'YYYY-MM-DD 00:00:00') and to_date(dateadd(day,-3*7,to_date(activity_feed,'YYYY-MM-DD', FALSE)),'YYYY-MM-DD 00:00:00')
)
group by cycl_time_id


UNION ALL






------ R12M --------


select 'Activity' as source,'R12M' as time_bckt_freq,
 cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'ROLLING 12 MONTHS' as time_bckt_nm
,'R12M' as time_bckt_cd
,'ANY' as indication
from
(select distinct 'R12M' as cd ,d.CAL_MTH_STRTDT as time_bckt_strt , d.CAL_MTH_ENDDT as time_bckt_end,c.cycl_time_id 
from eisai_dev_db.flg_sls as flg
cross join  eisai_dev_db.d_time d  
inner join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(month,(-12+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
and to_date(dateadd(month,(-1+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
)
group by cycl_time_id






UNION ALL





------ R12M Previous -------



select'Activity' as source,'R12M' as time_bckt_freq,
cast(min( time_bckt_strt ) as timestamp) as time_bckt_strt,cast(max( time_bckt_end ) as timestamp) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'ROLLING 12 MONTHS PREVIOUS' as time_bckt_nm
,'R12M(P)' as time_bckt_cd
,'ANY' as indication
from
(select distinct 'R12M' as cd ,d.CAL_MTH_STRTDT as time_bckt_strt , d.CAL_MTH_ENDDT as time_bckt_end,c.cycl_time_id  
from eisai_dev_db.flg_sls as flg
cross join  eisai_dev_db.d_time d  
inner join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD') between to_date(dateadd(month,(-24+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
and to_date(dateadd(month,(-13+flg.flg_m),to_date(activity_feed,'YYYY-MM-DD')),'YYYY-MM-DD 00:00:00') 
)
group by cycl_time_id



UNION ALL


----> QTD Previous <-----

select distinct source,time_bckt_freq, time_bckt_strt, b.cal_wk_enddt as x
-- Week end date required- Check 
--,dateadd(day,x,cal_qtr_strtdt) as x
,cycl_time_id, recency, time_bckt_nm, time_bckt_cd, indication  from 
(
SELECT 'Activity' as source, 'QUARTER TO DATE (WEEKLY)' as time_bckt_freq,  dateadd(month,-3,time_bckt_strt) AS time_bckt_strt , cycl_time_id ,
--datediff( day,time_bckt_strt,time_bckt_end) AS x ,
2 AS recency ,
'PREVIOUS QUARTER WEEKLY' AS time_bckt_nm,
'QTD2 (W)' AS time_bckt_cd,
'ANY' indication
,cal_wk_num_of_qtr
FROM 
(select 'Activity' as source,'QUARTER TO DATE (WEEKLY)' as time_bckt_freq,
  CAL_QTR_STRTDT as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end, cycl_time_id 
,1 as recency,'CURRENT QUARTER WEEKLY' as time_bckt_nm,'QTD1 (W)' as time_bckt_cd,
'ANY' as indication ,cal_wk_num_of_qtr
-- cal_wk_num_of_qtr    
from  eisai_dev_db.d_time d 
join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE) 
) as QTD 
)a left join eisai_dev_db.d_time b 
on a.time_bckt_strt=b.cal_qtr_strtdt
and a.cal_wk_num_of_qtr=b.cal_wk_num_of_qtr



UNION ALL





----> MTD Prev <----

select distinct source,time_bckt_freq, time_bckt_strt
,b.cal_wk_enddt as x

--, dateadd(day,x,cal_mth_strtdt) as x
-- Week end date required- Check 
,cycl_time_id, recency, time_bckt_nm, time_bckt_cd, indication  from 
(
SELECT 'Activity' as source,'MONTH TO DATE PREVIOUS' AS time_bckt_freq, dateadd(month,-1,time_bckt_strt) AS time_bckt_strt ,cycl_time_id, 
--datediff( day,time_bckt_strt,time_bckt_end) AS x ,
2 AS recency ,
'MONTH TO DATE PREVIOUS' AS time_bckt_nm, -- Change as per MTD
'MTD(P)' AS time_bckt_cd,
'ANY' as indication , cal_day_of_mth
FROM 
(
select 'Activity' as source,'MONTH TO DATE PREVIOUS' AS time_bckt_freq, cal_mth_strtdt as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end
,cycl_time_id,1 as recency,'MONTH TO DATE' as time_bckt_nm,'MTD' as time_bckt_cd,'ANY' as indication
,cal_day_of_mth    
from  eisai_dev_db.d_time d 
join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE)
) as MTD 
)a left join eisai_dev_db.d_time b 
on a.time_bckt_strt=b.cal_mth_strtdt
and a.cal_day_of_mth=b.cal_day_of_mth



UNION ALL







-----> YTD2 (W) <----------
select distinct source,time_bckt_freq, time_bckt_strt
,b.cal_wk_enddt as x

--, dateadd(day,x,cal_mth_strtdt) as x
-- Week end date required- Check 
,cycl_time_id, recency, time_bckt_nm, time_bckt_cd, indication from 
(
SELECT 'Activity' as source,'YEAR TO DATE (WEEKLY)' as time_bckt_freq, dateadd(year,-1,time_bckt_strt) AS time_bckt_strt ,cycl_time_id,
--datediff( day,time_bckt_strt,time_bckt_end) AS x ,
2 AS recency ,
'PREVIOUS YEAR TO DATE WEEKLY' AS time_bckt_nm, -- Change as per MTD
'YTD2 (W)' AS time_bckt_cd,'ANY' as indication, cal_day_of_yr  
FROM 
(
select 'Activity' as source,'YEAR TO DATE (WEEKLY)' as time_bckt_freq, cal_yr_strtdt as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end
, cycl_time_id,1 as recency,'PREVIOUS YEAR TO DATE WEEKLY' as time_bckt_nm,'YTD2 (W)' as time_bckt_cd, 'ANY' as indication, cal_day_of_yr   
from  eisai_dev_db.d_time d 
join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE)
) as YTD
)a left join eisai_dev_db.d_time b 
on a.time_bckt_strt=b.cal_yr_strtdt
and a.cal_day_of_yr=b.cal_day_of_yr


UNION ALL




----- Fiscal YTD (W) ---------

select distinct source,time_bckt_freq, time_bckt_strt,time_bckt_end, cycl_time_id, recency, time_bckt_nm, time_bckt_cd, indication from
( select 
case 
when datepart(year,dateadd(month, -3,  to_date(activity_feed,'YYYY-MM-DD', FALSE))) = datepart(year,dateadd(month, 0,  to_date(activity_feed,'YYYY-MM-DD', FALSE)))
then to_date(dateadd(month, 3, cal_yr_strtdt),'YYYY-MM-DD 00:00:00')
else to_date(dateadd(year,-1,dateadd(month, 3, cal_yr_strtdt)),'YYYY-MM-DD 00:00:00')end as time_bckt_strt,
'Activity' as source,'FISCAL YEAR TO DATE (WEEKLY)' as time_bckt_freq
, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end, cycl_time_id 
,1 as recency,'FISCAL YEAR TO DATE WEEKLY' as time_bckt_nm,'FYTD1 (W)' as time_bckt_cd, 'ANY' as indication  
from  eisai_dev_db.d_time d 
join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE) 
)


UNION ALL






-------> LTD <---------

select 'Activity' as source,'LAUNCH TO DATE (WEEKLY)' as time_bckt_freq,
'2021-07-23' as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'LAUNCH TO DATE WEEKLY' as time_bckt_nm
,'LTD (W)' as time_bckt_cd
,'EC' as indication 
from  eisai_dev_db.d_time d 
join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE)
 
UNION

select 'Activity' as source,'LAUNCH TO DATE (WEEKLY)' as time_bckt_freq,
'2021-08-12' as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as  time_bckt_end  ,
cycl_time_id,
1 as recency,
'LAUNCH TO DATE WEEKLY' as time_bckt_nm
,'LTD (W)' as time_bckt_cd
,'RCC' as indication 
from  eisai_dev_db.d_time d 
join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE) 


UNION ALL


---> QTD Current <---
select 'Activity' as source,'QUARTER TO DATE (WEEKLY)' as time_bckt_freq,
CAL_QTR_STRTDT as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end,
cycl_time_id 
,1 as recency,'CURRENT QUARTER WEEKLY' as time_bckt_nm,'QTD1 (W)' as time_bckt_cd,
'ANY' as indication    
from  eisai_dev_db.d_time d 
join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE) 

 

UNION ALL
 
 



---> YTD Current <---

select 'Activity' as source,'YEAR TO DATE (WEEKLY)' as time_bckt_freq,
 cal_yr_strtdt as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end,
 cycl_time_id 
,1 as recency,'YEAR TO DATE WEEKLY' as time_bckt_nm,'YTD (W)' as time_bckt_cd,
'ANY' as indication
-- cal_day_of_yr    
from  eisai_dev_db.d_time d 
join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE) 




UNION ALL




----> MTD Current <----

select 'Activity' as source,'CURRENT MONTH' as time_bckt_freq,
 cal_mth_strtdt as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end , cycl_time_id
,1 as recency,'MONTH TO DATE' as time_bckt_nm,'MTD' as time_bckt_cd,
'ANY' as indication 
--cal_day_of_mth    
from  eisai_dev_db.d_time d 
join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE)





UNION ALL



-----> WTD <--------

select 'Activity' as source,'CURRENT WEEK' as time_bckt_freq,
 cal_wk_strtdt as time_bckt_strt, to_date(activity_feed,'YYYY-MM-DD', FALSE) as time_bckt_end , cycl_time_id
,1 as recency,'WEEK TO DATE' as time_bckt_nm,'WTD' as time_bckt_cd,
'ANY' as indication 
--cal_day_of_mth    
from  eisai_dev_db.d_time d 
join eisai_dev_db.ctl_job_run c
on to_date(CAL_DT,'YYYY-MM-DD')=to_date(activity_feed,'YYYY-MM-DD', FALSE)

)


 





