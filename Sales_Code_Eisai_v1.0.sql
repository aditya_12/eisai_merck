drop table if exists eisai_dev_db.idl_eisai_sales;
create table eisai_dev_db.idl_eisai_sales
as 
select 
 coalesce(curr_sls.org             ,prev_sls.org              ) as org             
,coalesce(curr_sls.cntry_id        ,prev_sls.cntry_id         ) as cntry_id        
,coalesce(curr_sls.src_cd          ,prev_sls.src_cd           ) as src_cd          
,coalesce(curr_sls.channel         ,prev_sls.channel          ) as channel         
,coalesce(curr_sls.product_id      ,prev_sls.product_id       ) as product_id      
,coalesce(curr_sls.prod_name       ,prev_sls.prod_name        ) as prod_name       
,coalesce(curr_sls.brand_id        ,prev_sls.brand_id         ) as brand_id        
,coalesce(curr_sls.brand_name      ,prev_sls.brand_name       ) as brand_name      
,coalesce(curr_sls.indication      ,prev_sls.indication       ) as indication      
,coalesce(curr_sls.time_bckt_freq  ,prev_sls.time_bckt_freq   ) as time_bckt_freq  
,coalesce(curr_sls.time_bckt_cd    ,prev_sls.time_bckt_cd     ) as time_bckt_cd    --> Finally convert as per current time period
--,coalesce(curr_sls.time_bckt_id    ,prev_sls.time_bckt_id     ) as time_bckt_id    --> Finally convert as per current time period
,coalesce(curr_sls.time_bckt_nm    ,prev_sls.time_bckt_nm     ) as time_bckt_nm    --> Finally convert as per current time period
,coalesce(curr_sls.time_bckt_strt  ,prev_sls.time_bckt_strt   ) as time_bckt_strt  --> Finally convert as per current time period
,coalesce(curr_sls.time_bckt_end   ,prev_sls.time_bckt_end    ) as time_bckt_end   --> Finally convert as per current time period
,coalesce(curr_sls.recency         ,prev_sls.recency          ) as recency         --> Finally convert as per current time period

,coalesce(curr_sls.trx_goals       ,prev_sls.trx_goals        ) as trx_goals       
,coalesce(curr_sls.days_of_therapy ,prev_sls.days_of_therapy  ) as days_of_therapy 


,coalesce(curr_sls.trx             ,0  ) as curr_trx
,coalesce(curr_sls.nrx             ,0  ) as curr_nrx
,coalesce(prev_sls.trx             ,0  ) as prev_trx
,coalesce(prev_sls.nrx             ,0  ) as prev_nrx
,coalesce(curr_sls.trx             ,0  ) - coalesce(prev_sls.trx             ,0  ) as trx_diff
,coalesce(curr_sls.nrx             ,0  ) - coalesce(prev_sls.nrx             ,0  ) as nrx_diff

from 
(select 
org
,cntry_id
,src_cd
--,split_week_end_date
,channel -- Place holder
,product_id
,prod_name
,brand_id
,brand_name
,indication
,time_bckt_freq
,time_bckt_cd
--,time_bckt_id
,time_bckt_nm
,time_bckt_strt
,time_bckt_end
,recency
,max(trx_goals) as trx_goals
,sum(trx) as trx
,sum(nrx) as nrx
,max(days_of_therapy) as days_of_therapy
--,date_of_delivery
--,split_week_end

from eisai_dev_db.f_eisai_sales_crnt_time_prd 
group by org
,cntry_id
,src_cd
--,split_week_end_date
,channel -- Place holder
,product_id
,prod_name
,brand_id
,brand_name
,indication
,time_bckt_freq
,time_bckt_cd
--,time_bckt_id
,time_bckt_nm
,time_bckt_strt
,time_bckt_end
,recency
) as curr_sls 


full join 

(
select 
org
,cntry_id
,src_cd
--,split_week_end_date
, channel -- Place holder
,product_id
,prod_name
,brand_id
,brand_name
,indication
,curr_time_bckt_freq as time_bckt_freq
,curr_time_bckt_cd   as time_bckt_cd
--,curr_time_bckt_id   as time_bckt_id
,curr_time_bckt_nm   as time_bckt_nm
,curr_time_bckt_strt as time_bckt_strt
,curr_time_bckt_end  as time_bckt_end
,curr_recency        as recency
, trx_goals
, trx
, nrx
, days_of_therapy
from
(
select 
org
,cntry_id
,src_cd
--,split_week_end_date
,channel -- Place holder
,product_id
,prod_name
,brand_id
,brand_name
,indication
,time_bckt_freq
,time_bckt_cd
--,time_bckt_id
,time_bckt_nm
,time_bckt_strt
,time_bckt_end
,recency
,max(trx_goals) as trx_goals
,sum(trx) as trx
,sum(nrx) as nrx
,max(days_of_therapy) as days_of_therapy
--,date_of_delivery
--,split_week_end

from eisai_dev_db.f_eisai_sales_prev_time_prd 
group by org
,cntry_id
,src_cd
--,split_week_end_date
,channel -- Place holder
,product_id
,prod_name
,brand_id
,brand_name
,indication
,time_bckt_freq
,time_bckt_cd
--,time_bckt_id
,time_bckt_nm
,time_bckt_strt
,time_bckt_end
,recency
) as prev
left join 
 
(
select distinct time_bckt_freq as curr_time_bckt_freq ,time_bckt_cd as curr_time_bckt_cd ,time_bckt_nm as curr_time_bckt_nm ,time_bckt_strt as curr_time_bckt_strt ,time_bckt_end as curr_time_bckt_end ,recency as curr_recency
from eisai_dev_db.f_eisai_sales_crnt_time_prd 
where time_bckt_cd in ('R4W', 'R13W', 'WTD', 'MTD', 'QTD1 (W)', 'YTD (W)', 'M1', 'R12M')
) as curr
on curr.curr_time_bckt_freq=prev.time_bckt_freq





) as prev_sls 

on  curr_sls.org              =   prev_sls.org
and curr_sls.cntry_id         =   prev_sls.cntry_id
and curr_sls.src_cd           =   prev_sls.src_cd
and curr_sls.channel          =   prev_sls.channel 
and curr_sls.product_id       =   prev_sls.product_id
and curr_sls.prod_name        =   prev_sls.prod_name
and curr_sls.brand_id         =   prev_sls.brand_id
and curr_sls.brand_name       =   prev_sls.brand_name
and curr_sls.indication       =   prev_sls.indication
and curr_sls.time_bckt_freq   =   prev_sls.time_bckt_freq
and curr_sls.time_bckt_cd     =   prev_sls.time_bckt_cd
--and curr_sls.time_bckt_id     =   prev_sls.time_bckt_id
and curr_sls.time_bckt_nm     =   prev_sls.time_bckt_nm
and curr_sls.time_bckt_strt   =   prev_sls.time_bckt_strt
and curr_sls.time_bckt_end    =   prev_sls.time_bckt_end
and curr_sls.recency          =   prev_sls.recency
